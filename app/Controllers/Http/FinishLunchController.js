'use strict'

const ChooseDeliveryGuyService = use('App/Services/ChooseDeliveryGuyService')
const FinishLunchService = use('App/Services/FinishLunchService')

class FinishLunchController {
  async finish({request, response}) {
    const finishService = new FinishLunchService();
    await finishService.finish()

    const service = new ChooseDeliveryGuyService();
    await service.choose()

    return 'Finalizado';
  }
}

module.exports = FinishLunchController
