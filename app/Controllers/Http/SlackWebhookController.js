'use strict'

const { WebClient } = require('@slack/web-api');
// Read a token from the environment variables
const token = process.env.SLACK_TOKEN;

// Initialize
const web = new WebClient(token);
const request = require('request');
const Member = use('App/Models/Member')
const Response = use('App/Models/Response')
const LunchOption = use('App/Models/LunchOption')


class SlackWebhookController {
  async webhook({request}) {
    // slackInteractions.requestListener()
    const payload = JSON.parse(request.input('payload'))
    //console.log(payload)
    let member = await Member.findOrCreate(
      {slack_id: payload.user.id},
      {slack_id: payload.user.id, username: payload.user.name},
    )

    // console.log('member', member)
    //console.log(payload.actions[0]);
    let theOption = await LunchOption.findOrFail(payload.actions[0].value);
    await theOption.load('lunch');
    let theLunch = await theOption.getRelated('lunch')
    if (theLunch.isClosed()) {
      this.sendMessageToSlackResponseURL(payload.response_url, {
        response_type: 'ephemeral',
        replace_original: false,
        text: ':no_entry_sign: Este almuerzo ya está cerrado. No se puede modificar tu elección'
      })
      return
    }
    const previousSelection = await member.responses()
      .whereHas('lunch_option.lunch', (builder) => {
        builder.where('lunch_id', theOption.lunch_id)
      })
      .with('lunch_option')
    .first()

    let newSelection = await member.responses().create({
      lunch_option_id: theOption.id,
    });
    await newSelection.load('lunch_option')

    //console.log(previousSelection, newSelection)
    let deleted = false;
    if (previousSelection !== null) {
      // Este usuario ya contestó este almuerzo
      console.log('Ya contestó este almuerzo. Reemplazando respuesta');
      if (previousSelection.lunch_option_id === newSelection.lunch_option_id) {
        this.sendMessageToSlackResponseURL(payload.response_url, {
          response_type: 'ephemeral',
          replace_original: false,
          text: ':no_entry_sign: Eliminando tu elección. Recuerda seleccionar tu opción antes de las 11.30am'
        })
        deleted = true;
        await newSelection.delete();
        await previousSelection.delete();
      } else {
        this.sendMessageToSlackResponseURL(payload.response_url, {
          response_type: 'ephemeral',
          replace_original: false,
          text: 'Cambiando tu elección de almuerzo. \n\n:no_entry_sign: ~*Anterior:* ' + previousSelection.getRelated('lunch_option').option + '~\n\n:star: *Nueva opción:* ' + newSelection.getRelated('lunch_option').option
        })
        deleted = true;
        await previousSelection.delete();
      }
    }

    if (!deleted) {
      this.sendMessageToSlackResponseURL(payload.response_url, {
        response_type: 'ephemeral',
        text: ':star: *Has elegido:* ' + newSelection.getRelated('lunch_option').option,
        replace_original: false,
        // attachments: [{
        //   text: '¿Deseas sumar pan?',
        //   actions: [{
        //     name: 'add_bread',
        //     text: ':baguette_bread: Agregar pan',
        //     type: 'button',
        //     value: 'bread'
        //   }]
        // }]
      })
    }


    let newMessage = payload.original_message;

    let lunchOptions = await LunchOption.query().where('lunch_id', theOption.lunch_id).with('responses.member').fetch()
    let attachments = lunchOptions.rows.map((option, index) => {
      const responses = option.getRelated('responses').rows
      index++
      let text = '*Opción #' + index + '* ';
      if (responses.length) text += '`' + responses.length + '`';
      text += '\n' + responses.map(response => '<@' + response.getRelated('member').slack_id + '>').join(',');
      text += '\n' + option.option;

      return {
        text,
        'fallback': 'Escoger esta opción de menú',
        'callback_id': option.lunch_id,
        'color': '#3AA3E3',
        'attachment_type': 'default',
        'actions': [{
          'name': 'menu',
          'text': 'Elegir esta opción',
          'type': 'button',
          'value': option.id,
        }]
      }
    })

    // console.log('attachments', attachments)


    return {
      "response_type": "ephemeral",
      "replace_original": true,
      attachments: attachments,
      "text": newMessage.text
    };
  }


  sendMessageToSlackResponseURL(responseURL, JSONmessage){
    request.post({ uri: responseURL, json: true, body: JSONmessage }, (err, res, body) => {
      if (err) {
        return console.log(err);
      }
      console.log(body);
    });
  }
}

module.exports = SlackWebhookController
