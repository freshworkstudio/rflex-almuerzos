'use strict'
const {IncomingWebhook} = require('@slack/webhook')
const Lunch = use('App/Models/Lunch')
let moment = require('moment')

// Read a url from the environment variables
const url = process.env.SLACK_WEBHOOK_URL

// Initialize
const slackWebhook = new IncomingWebhook(url)

class ReceiveLunchMenuController {
  async webhook ({request, response}) {
    let data = request.all()
    // console.log(data)
    const body = data.Body;

    if (body === 'sortear') {
      const ChooseDeliveryGuyService = use('App/Services/ChooseDeliveryGuyService')
      const FinishLunchService = use('App/Services/FinishLunchService')
      const finishService = new FinishLunchService();
      const lunch = await finishService.finish()

      const service = new ChooseDeliveryGuyService();
      await service.choose()
      return 'Sorteo realizado';
    }



    let options = body.split(/([0-9])[.-]+/)
    options = this.removeGreeting(options)
      .filter(option => isNaN(option))
      .map(option => option.replace(/\\n/g, '\n'))
      .map(option => option.trim())
      .map(option => {
        let lines = option.split('\n')
        //lines[0] = '*' + lines[0] + '*'
        return lines.join('\n')
      })
    if (options.length <= 0) {
      console.log('No encontré opciones en el menú');
      return 'Lo siento. No entendí tu mensaje. Si enviaste el menú, no pude reconocer automáticamente las opciones que había para almuerzos. Avísale a un humano para que pueda tomar el pedido correctamente. ';
    }

    const anotherLunchOfToday = await Lunch.query().orderBy('id', 'desc').where('date', moment().format('YYYY-MM-DD')).first()
    if (anotherLunchOfToday) {
      slackWebhook.send({
        text: 'El pedido anterior ha sido cerrado, ya que llegó un nuevo almuerzo para hoy'
      })
      anotherLunchOfToday.close()
    }

    let lunch = new Lunch()
    lunch.date = new Date()
    lunch.message = data.Body
    await lunch.save()

    let slackButtons = []

    for (const index in options) {
      const option = options[index]
      console.log(index, option)
      let theOption = await lunch.options().create({
        option
      })
      slackButtons.push({
        'text': '*Opción #' + (index + 1) + '*\n' + option,
        'fallback': 'Escoger esta opción de menú',
        'callback_id': lunch.id,
        'color': '#3AA3E3',
        'attachment_type': 'button',
        'actions': [{
          'name': 'menu',
          'text': 'Elegir esta opción',
          'type': 'button',
          'value': theOption.id,
        }]
      })
    }

    const icon =  [':male-cook:', ':female-cook:'][this.randomIntFromInterval(0, 1)];
    slackWebhook.send({
      text: '<!channel>\n\nEste es el menú de hoy. Elige tu opción antes de las 10:00 am ' + icon,
      username: 'Roboc rFlex de la comida',
      icon_emoji: icon,
      'attachments': slackButtons
    })

    return 'Gracias por enviar el menú. Estamos organizado en pedido. Apenas nos organicemos, le enviaré el resumen. '
  }

  removeGreeting (options) {
    if (options.length <= 0) return options;
    if (isNaN(options[0])) {
      options.shift()
      return this.removeGreeting(options)
    }
    return options
  }

  randomIntFromInterval (min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

}

module.exports = ReceiveLunchMenuController
