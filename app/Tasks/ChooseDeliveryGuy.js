'use strict'

const Task = use('Task')
const ChooseDeliveryGuyService = use('App/Services/ChooseDeliveryGuyService')

class ChooseDeliveryGuy extends Task {
  static get schedule () {
    return '0 1 13 * * *'
    // return '*/10 * * * * *'
  }

  async handle () {

    const service = new ChooseDeliveryGuyService();
    await service.choose()

    this.info('Task ChooseDeliveryGuy handle')
  }
}

module.exports = ChooseDeliveryGuy
