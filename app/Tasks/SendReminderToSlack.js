'use strict'

const Task = use('Task')
const {IncomingWebhook} = require('@slack/webhook')
const Lunch = use('App/Models/Lunch')
let moment = require('moment')

// Read a url from the environment variables
const url = process.env.SLACK_WEBHOOK_URL

// Initialize
const slackWebhook = new IncomingWebhook(url)

class SendReminderToSlack extends Task {
  static get schedule () {
    return '0 45 12 * * *'
  }

  async handle () {

    const Lunch = use('App/Models/Lunch')

    const lunch = await Lunch.query().orderBy('date', 'desc').whereNull('closed_at').with('options.responses').first()
    if (lunch === null) {
      console.log('No hay almuerzos abiertos. No enviar recordatorio.')
      return;
    }

    slackWebhook.send({
      text: '<!channel> Quedan 15 minutos para cerrar el pedido... :timer_clock:'
    })
  }
}

module.exports = SendReminderToSlack
