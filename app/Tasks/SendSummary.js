'use strict'
let moment = require('moment')
const Task = use('Task')
const {IncomingWebhook} = require('@slack/webhook')
const FinishLunchService = use('App/Services/FinishLunchService')
const GONZALO='+56988153776'
const HECTOR='+56949818382'
const FRANCO='+56954465129'
const receivers = [GONZALO, HECTOR, FRANCO];

// Read a url from the environment variables
const url = process.env.SLACK_WEBHOOK_URL

// Initialize
const slackWebhook = new IncomingWebhook(url)

class SendSummary extends Task {
  static get schedule () {
    return '0 0 13 * * *'
    // return '* * * * * *'
  }

  async handle () {
    const finishService = new FinishLunchService();
    const lunch = await finishService.finish()
    if (lunch === null) {
      return;
    }

    const options = lunch.getRelated('options').toJSON().filter(option => option.responses.length);

    let greeting = 'Hola Giselle, hoy en rFlex pediremos:\n ';
    let message = '';
    for(const i in options) {
      const option = options[i]
      message += '*' + option.responses.length + 'x* ' + option.option.split('\n')[0] + '\n';
    }
    //@TODO: Pedir panes desde la BD
    message += '*3x* panes \n';
    console.log(greeting + message)

    slackWebhook.send({
      text: '<!channel>\n\nEl pedido de hoy ha finalizado :rocket: \n\n' + message,
      username: 'Roboc rFlex de la comida'
    })

    const client = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_AUTH_TOKEN);

    for(const receiver of receivers) {
      client.messages
      .create({
        from: 'whatsapp:+14155238886',
        body: greeting + message,
        to: 'whatsapp:' + receiver
      })
      .then(message => console.log(message.sid));
    }

    //lunch.options().responses()
    this.info({
      message: 'Task SendSummary handle'
    })
  }
}

module.exports = SendSummary
