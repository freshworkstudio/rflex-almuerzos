'use strict';

module.exports = class FinishLunchService {
  async finish() {
    const Lunch = use('App/Models/Lunch')

    const lunch = await Lunch.query().orderBy('id', 'desc').whereNull('closed_at').with('options.responses').first()
    if (lunch === null) {
      console.log('No hay almuerzos abiertos')
      return null;
    }
    lunch.close();

    return lunch;
  }
}
