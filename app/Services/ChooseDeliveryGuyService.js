'use strict'

const Task = use('Task')
const {IncomingWebhook} = require('@slack/webhook')
const Lunch = use('App/Models/Lunch')
let moment = require('moment')

// Read a url from the environment variables
const url = process.env.SLACK_WEBHOOK_URL

// Initialize
const slackWebhook = new IncomingWebhook(url)


module.exports = class ChooseDeliveryGuyService {
  constructor () {

  }
  async choose() {
    let membersWithResponses = []
    const lunch = await Lunch.query().orderBy('closed_at', 'desc').whereNotNull('closed_at').whereNull('delivery_guy_id').with('options.responses.member').first()

    if (lunch === null) {
      console.log('No hay almuerzos abiertos sin persona elegida. No realizar sorteo.')
      return;
    }

    const options = lunch.getRelated('options').toJSON().filter(option => option.responses.length);

    for (const i in options) {
      const option = options[i]
      for (const r in option.responses) {
        const response = option.responses[r]
        membersWithResponses.push(response.member)
      }
    }

    // console.log(membersWithResponses);
    const winner = membersWithResponses[Math.floor(Math.random() * membersWithResponses.length)];

    lunch.setDeliveryGuy(winner.id);

    slackWebhook.send({
      text: '¿Quién irá a buscar el pedido? Lo sabremos en... '
    })

    await timeout(1000);

    slackWebhook.send({
      text: '3...'
    })

    await timeout(1000);

    slackWebhook.send({
      text: '2...'
    })

    await timeout(1000);

    slackWebhook.send({
      text: '1...'
    })

    await timeout(1000);

    slackWebhook.send({
      text: 'Tenemos ganador: <@' + winner.slack_id + '> '
    })


    await timeout(1000);

  }
}

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
