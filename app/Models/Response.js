'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Response extends Model {
  lunch_option() {
    return this.belongsTo('App/Models/LunchOption')
  }

  member() {
    return this.belongsTo('App/Models/Member')
  }
}

module.exports = Response
