'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Member extends Model {
  responses() {
    return this.hasMany('App/Models/Response')
  }
}

module.exports = Member
