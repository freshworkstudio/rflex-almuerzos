'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LunchOption extends Model {
  lunch() {
    return this.belongsTo('App/Models/Lunch')
  }

  responses() {
    return this.hasMany('App/Models/Response')
  }
}

module.exports = LunchOption
