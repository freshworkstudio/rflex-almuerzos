'use strict'

const moment = require('moment')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Lunch extends Model {

  static get dates () {
    return super.dates.concat(['date' ,'closed_at'])
  }

  options() {
    return this.hasMany('App/Models/LunchOption')
  }

  isClosed() {
    return this.closed_at !== null
  }

  close() {
    this.closed_at = moment()
    this.save()
  }

  setDeliveryGuy(member_id) {
    this.delivery_guy_id = member_id
    this.save()
  }
}

module.exports = Lunch
