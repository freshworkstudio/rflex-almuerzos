'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LunchsSchema extends Schema {
  up () {
    this.create('lunches', (table) => {
      table.increments()
      table.date('date')
      table.string('supplier').nullable()
      table.date('slack_message_id')
      table.text('message')
      table.timestamp('closed_at').nullable()
      table.integer('delivery_guy_id').nullable().unsigned()
      table.timestamps()

      table.foreign('delivery_guy_id').references('id').inTable('members')
    })
  }

  down () {
    this.drop('lunches')
  }
}

module.exports = LunchsSchema
