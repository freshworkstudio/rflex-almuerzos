'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResponsesSchema extends Schema {
  up () {
    this.create('responses', (table) => {
      table.increments()
      table.integer('lunch_option_id').unsigned()
      table.foreign('lunch_option_id').references('id').inTable('lunch_options')

      table.integer('member_id').unsigned()
      table.foreign('member_id').references('id').inTable('members')

      table.text('comments').nullable()
      table.boolean('bread').default(false)
      table.json('meta')
      table.timestamps()
    })
  }

  down () {
    this.drop('responses')
  }
}

module.exports = ResponsesSchema
