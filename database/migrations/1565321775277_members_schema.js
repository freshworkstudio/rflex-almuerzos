'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MembersSchema extends Schema {
  up () {
    this.create('members', (table) => {
      table.increments()
      table.string('first_name').nullable()
      table.string('last_name').nullable()
      table.string('username')
      table.string('slack_id')
      table.string('phone').nullable()
      table.string('email').nullable()
      table.string('nice_name').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('members')
  }
}

module.exports = MembersSchema
