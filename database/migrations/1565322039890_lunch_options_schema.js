'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LunchOptionsSchema extends Schema {
  up () {
    this.create('lunch_options', (table) => {
      table.increments()
      table.integer('lunch_id').unsigned()
      table.string('option')
      table.text('description')
      table.timestamps()

      table.foreign('lunch_id').references('id').inTable('lunches')
    })
  }

  down () {
    this.drop('lunch_options')
  }
}

module.exports = LunchOptionsSchema
